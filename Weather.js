const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{
    /**
     * Describe attributes: temperature, wind, date, forecast
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = Date.now()
        this.description = ''
        this.forecast = []
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
            this.description = response.data.description
            this.forecast1 = response.data.forecast[0]
            this.forecast2 = response.data.forecast[1]
            this.forecast3 = response.data.forecast[2]
        }).catch((error) => {return error.message})
    }

    //async setForecast(cityName){
        
       // await axios.get(weatherUrl + cityName).then((response) => {
            //this.forecast1 = response.data.forecast[0] 
            //this.forecast2 = response.data.forecast[1]   
            //this.forecast3 = response.data.forecast[2]   
       // }).catch((error) => {return error.message})
   // }


}

module.exports = {Weather}