const {Country} = require('./Country')
const {City} = require('./City')
const {Capital} = require('./Capital')

let city1 = new City('Manchester')
let city2 = new City('Birmingham')
let city3 = new City('Liverpool')
let city4 = new City('Nottingham')
let city5 = new City('Sheffield')
let city6 = new City('Bristol')
let city7 = new City('Glasgow')
let city8 = new City('Newcastle')
let city9 = new City('Leeds')
let city10 = new City('Southampton')

city1.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city2.setWeather().then(() => {console.log(city2)}).catch((error) => {console.log(error)})
city3.setWeather().then(() => {console.log(city3)}).catch((error) => {console.log(error)})
city4.setWeather().then(() => {console.log(city4)}).catch((error) => {console.log(error)})
city5.setWeather().then(() => {console.log(city5)}).catch((error) => {console.log(error)})
city6.setWeather().then(() => {console.log(city6)}).catch((error) => {console.log(error)})
city7.setWeather().then(() => {console.log(city7)}).catch((error) => {console.log(error)})
city8.setWeather().then(() => {console.log(city8)}).catch((error) => {console.log(error)})
city9.setWeather().then(() => {console.log(city9)}).catch((error) => {console.log(error)})
city10.setWeather().then(() => {console.log(city10)}).catch((error) => {console.log(error)})

let cities = [city1, city2, city3, city4, city5, city6, city7, city8, city9, city10]
//console.log(cities)

//cities.setWeather().then(() => {console.log(cities)}).catch((error) => {console.log(error)})

//city1.setForecast().then(() => {console.log(city1)}).catch((error) => {console.log(error)})

let capital = new Capital('London')
capital.setAirport('Luton')
capital.setWeather().then(() => {console.log(capital)}).catch((error) => {console.log(error)})

let country = new Country('England')
console.log(country)

addCity = cities.push('Bradford')
//console.log(cities)

let index = cities.findIndex(city => city === 'Southampton')
//console.log(index)
if (index !== -1) {
    cities.splice(index, 1);  
}
//console.log(cities)

//cities.sort(compareTemp)
//function compareTemp(a, b) {
   // if(a.City.weather.temperature < b.City.weather.temperature)
   // return 1
  //  if(a.City.weather.temperature > b.City.weather.temperature)
   // return -1
   // return 0
//}
//console.log(cities)